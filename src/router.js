/* eslint-disable import/no-named-as-default */
import React from "react";
import { Route, Switch, BrowserRouter } from "react-router-dom";
import HomePageComponent from "./components/HomePageComponent";
import PropTypes from "prop-types";
import { hot } from "react-hot-loader";

class AppRouter extends React.Component {
  render() {
    return (
      <>
        <BrowserRouter>
          <Switch>
            <Route exact path="/" component={HomePageComponent} />
            {/* <Route component={NotFoundPage} /> */}
          </Switch>
        </BrowserRouter>
      </>
    );
  }
}

AppRouter.propTypes = {
  children: PropTypes.element,
};

export default hot(module)(AppRouter);
