import React from "react";
import { render } from "react-dom";
import { createStore, compose, applyMiddleware } from "redux";
import thunkMiddleware from "redux-thunk";
import { Provider } from "react-redux";

import "./styles/index.scss";

import AppRouter from "./router";

import Reducers from "./store/reducers";

let middlewares = [thunkMiddleware];
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
  Reducers,
  composeEnhancers(applyMiddleware(...middlewares))
);

render(
  <Provider store={store}>
    <AppRouter />
  </Provider>,
  document.getElementById("app")
);
