// Set up your root reducer here...
import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router'
import mainReducer from './mainReducer';

 const rootReducer = history => combineReducers({
  router: connectRouter(history),
  mainReducer
});

 export default rootReducer;