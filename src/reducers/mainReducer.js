import initialState from './initialState';

export default function mainReducer(state = initialState, action) {
  switch (action.type) {

    case 'OPEN-SIDEBAR': return {
      // preciso que o estado seja true p poder abrir
      // depois criar outra action p retornar pra false e fechar
      // (mas isso vai ter que ser em outro componente)
      ...state,
      click : !state.click
    }

    case 'ATUALIZA-LISTA': return [
      ...state,
      ...action.listaAt
    ]

    case 'LISTA-SELECIONADAS': return [
      ...state,
      state.objectId
    ]
    case 'PASS-IS-CONFIRMED': return {
      passIsConfirmed: !state.passIsConfirmed
    }

    default: return state;
  }
}
