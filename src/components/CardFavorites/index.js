import React from "react";
import Trash from "../../assets/img/trash.svg";

const CardFavorites = ({ fave, onRemoved }) => {
  return (
    <div className="cardFavoritas">
      <div className="dados">
        <p>Origem: {fave.Origin}</p>
        <p>Destino: {fave.Destination}</p>
        <p>Embarque: {fave.DepartureDate.iso}</p>
      </div>

      <div className="preco">
        <h2>R$ {fave.Price}</h2>
        <p>{fave.BusClass}</p>
      </div>
      <button
        onClick={() => {
          onRemoved(fave.objectId);
        }}
      >
        <img src={Trash} alt="Exclua esse item" />
      </button>
    </div>
  );
};

export default CardFavorites;
