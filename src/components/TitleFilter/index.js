import React, { Component } from "react";
import BusImg from "../../assets/img/bus.png";

class TitleFilter extends Component {
  render() {
    return (
      <div className="titulo-filtro main">
        <div className="container">
          <img src={BusImg} alt="ícone de um ônibus" />
          <div className="titles">
            <h1>Passagem de ônibus de São Paulo para Rio de Janeiro</h1>
            <p>Data de embarque: 08/09/2019</p>
          </div>
        </div>
      </div>
    );
  }
}
export default TitleFilter;
