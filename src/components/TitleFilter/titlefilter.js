import React from 'react';
import BusImg from '../../assets/img/bus.png';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';

const useStyles = makeStyles({
    content: {
        display: 'flex',
        justifyContent: 'center',
        padding: '3vw 0',
        backgroundColor: '#DEDEDE',
    },
    container: {
        width: '80vw',
        display: 'flex',
        paddingLeft: '0 !important'
    },
    img: {
        width: '35px',
        height: '35px',
        padding: '1vw'
    },
    titles: {
        paddingLeft: '2vw',
        paddingTop: '5px'
    },
    toFrom: {
        color: '#3C3C3C',
        fontSize: '28px',
    },
    date: {
        color: '#747474',
        fontSize: '14px',
        letterSpacing: 0,
    },
});

const TitleFilter = () => {
    const classes = useStyles()
    return (
        <div className={classes.content}>
            <Container className={classes.container}>
                <img className={classes.img} src={BusImg} alt="ícone de um ônibus" />
                <div className={classes.titles}>
                    <h1 className={classes.toFrom}>Passagem de ônibus de São Paulo para Rio de Janeiro</h1>
                    <p className={classes.date}>Data de embarque: 08/09/2019</p>
                </div>
            </Container>
        </div>
    )
}

export default TitleFilter;