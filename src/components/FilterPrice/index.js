import React from "react";

const FilterPrice = () => {
  return (
    <div className="filterprice">
      <input type="range" min="5" max="10" step="0.01" />
    </div>
  );
};
export default FilterPrice;

// import React from "react";
// import Slider from "@material-ui/core/Slider";
// import { makeStyles } from "@material-ui/core/styles";

// const useStyles = makeStyles({
//   slider: {
//     color: "#E61D41",
//     paddingBottom: "9px",
//   },
//   title: {
//     color: "#3C3C3C",
//   },
//   price: {
//     paddingTop: "7px",
//   },
// });

// export const FilterPrice = () => {
//   const classes = useStyles();
//   return (
//     <div className={classes.title}>
//       <strong>Preço máximo</strong>
//       <br></br>
//       <p className={classes.price}>R$ 254,90</p>
//       <Slider
//         className={classes.slider}
//         aria-label="Filtro visual de preço de passagens"
//         defaultValue={20}
//       />
//     </div>
//   );
// };
