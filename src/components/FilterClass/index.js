import React from "react";
import Checkbox from "@material-ui/core/Checkbox";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
  root: {
    padding: "3px 3px 3px 6px!important",
  },
  title: {
    color: "#3C3C3C",
  },
});

export const FilterClass = (props) => {
  const classes = useStyles();
  const { class_filter, isFiltred } = props;

  return (
    <div className={classes.title}>
      <strong>Classe</strong>
      <form className={classes.root}>
        <FormGroup>
          <FormControlLabel
            control={
              <Checkbox
                className={classes.root}
                checked={class_filter.convencional}
                onChange={isFiltred}
                value="Convencional"
                name="convencional"
                inputProps={{ "aria-label": "Seleção de classe convencional" }}
              />
            }
            label="Convencional"
          />
          <FormControlLabel
            control={
              <Checkbox
                className={classes.root}
                checked={class_filter.executivo}
                onChange={isFiltred}
                value="Executivo"
                name="executivo"
                inputProps={{ "aria-label": "Seleção de classe executiva" }}
              />
            }
            label="Executivo"
          />
          <FormControlLabel
            control={
              <Checkbox
                className={classes.root}
                checked={class_filter.semileito}
                onChange={isFiltred}
                value="Semi Leito"
                name="semileito"
                inputProps={{ "aria-label": "Seleção de classe semi-leito" }}
              />
            }
            label="Semi-leito"
          />
          <FormControlLabel
            control={
              <Checkbox
                className={classes.root}
                checked={class_filter.leito}
                onChange={isFiltred}
                value="Leito"
                name="leito"
                inputProps={{ "aria-label": "Seleção de classe leito" }}
              />
            }
            label="Leito"
          />
        </FormGroup>
      </form>
    </div>
  );
};
