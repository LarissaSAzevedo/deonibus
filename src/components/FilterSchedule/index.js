import React from "react";
import Checkbox from "@material-ui/core/Checkbox";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
  root: {
    padding: "3px 3px 3px 6px!important",
  },
  title: {
    color: "#3C3C3C",
  },
});

export const FilterSchedule = (props) => {
  const classes = useStyles();
  const { exit_filter, isFiltred } = props;
  return (
    <div className={classes.title}>
      <strong>Saída</strong>
      <form className={classes.root}>
        <FormGroup>
          <FormControlLabel
            control={
              <Checkbox
                className={classes.root}
                checked={exit_filter.madrug}
                onChange={isFiltred}
                value="madrugada"
                name="madrugada"
                inputProps={{ "aria-label": "Seleção de saída de madrugada" }}
              />
            }
            label="Madrugada - (00h00 - 05h59)"
          />

          <FormControlLabel
            control={
              <Checkbox
                className={classes.root}
                checked={exit_filter.manha}
                onChange={isFiltred}
                value="manha"
                name="manha"
                inputProps={{ "aria-label": "Seleção de saída de manhã" }}
              />
            }
            label="Manhã - (06h00 - 11h59)"
          />

          <FormControlLabel
            control={
              <Checkbox
                className={classes.root}
                checked={exit_filter.tarde}
                onChange={isFiltred}
                value="tarde"
                name="tarde"
                inputProps={{ "aria-label": "Seleção de saída de tarde" }}
              />
            }
            label="Tarde - (12h00 - 17h59)"
          />

          <FormControlLabel
            control={
              <Checkbox
                className={classes.root}
                checked={exit_filter.noite}
                onChange={isFiltred}
                value="noite"
                name="noite"
                inputProps={{ "aria-label": "Seleção de saída de noite" }}
              />
            }
            label="Noite - (18h00 - 23h59)"
          />
        </FormGroup>
      </form>
    </div>
  );
};
