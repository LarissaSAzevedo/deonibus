import React from "react";

const TableData = ({ selected, onSelected, route }) => {
  const {
    Company,
    DepartureDate,
    Origin,
    Destination,
    BusClass,
    Price,
    objectId,
  } = route;
  return (
    <div className="card">
      <ul>
        <li>{Company.Name}</li>
        <li>{DepartureDate.iso}</li>
        <li>
          {Origin} <br></br> {Destination}
        </li>
        <li>2h 30min</li>
        <li>{BusClass}</li>
        <li>
          <strong>R$ {Price.toLocaleString("pt-BR")}</strong>
        </li>
        <li>
          {selected.find((t) => t.objectId == objectId) ? (
            <p>Escolhido</p>
          ) : (
            <button id="btn" onClick={() => onSelected(route)}>
              <strong>Escolher</strong>
            </button>
          )}
        </li>
      </ul>
    </div>
  );
};
export default TableData;
