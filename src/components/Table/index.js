import React from "react";
import TableData from "../TableData";

export default function Table({ ticket, favorites, selected, onSelected }) {
  return (
    <div>
      <div className="table top-box">
        <ul>
          <li className="hide">Empresa</li>
          <li>Saída</li>
          <li>Embarque/Desembarque</li>
          <li>Duração</li>
          <li>Classe</li>
          <li>Preço</li>
          <li className="hide">Opção</li>
        </ul>
      </div>

      {ticket.data.map((eachRoute) => {
        if (favorites.find((f) => f.objectId === eachRoute.objectId)) {
          return null;
        } else {
          return (
            <TableData
              route={eachRoute}
              onSelected={onSelected}
              selected={selected}
              key={eachRoute.objectId}
            />
          );
        }
      })}
    </div>
  );
}
