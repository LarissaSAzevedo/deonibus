import React, { Component } from "react";
import { connect } from "react-redux";

import { ticketAction } from "../store/actions";

import Header from "./Header";
import TitleFilter from "./TitleFilter";
import Table from "./Table";
import TableData from "./TableData";
import FilterBox from "./FilterBox";
import SelectedPass from "./SelectedPass";
import Sidebar from "./Sidebar";

class HomePageComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      routes: [],
      favorites: [],
      selected: [],
      fave: [],
      sidebarIsOpen: false,
      isConfirmed: false,

      class_filter: {
        conventional: false,
        executive: false,
        bed: false,
        semi_bed: false,
      },

      exit_filter: {
        dawn: false,
        morning: false,
        evening: false,
        night: false,
      },
      passList: [],
    };
  }

  componentDidMount() {
    this.getTickets();
  }

  onSelected = (route) => {
    this.setState({
      selected: this.state.selected.concat(route),
    });
  };

  handleOpenSidebar = () => {
    this.setState({
      sidebarIsOpen: !this.state.sidebarIsOpen,
    });
  };

  onConfirmed = () => {
    this.setState({
      isConfirmed: true,
      favorites: this.state.favorites.concat(this.state.selected),
      selected: [],
    });
  };

  onRemoved = (fave) => {
    const favorites = this.state.favorites.filter((f) => {
      return f.objectId !== fave; //retorna só os itens diferentes do que já tem e vai ser removido
    });
    this.setState({
      favorites,
    });
    this.onEmpty();
  };

  onEmpty = () => {
    if (this.state.favorites === []) {
      this.setState({
        isConfirmed: false,
      });
    }
  };

  onFiltered = (event) => {
    const target = event.target;
    const value = target.type === "checkbox" ? target.checked : target.value;
    const name = target.name;

    this.setState({
      class_filter: {
        ...this.state.class_filter,
        [name]: value,
      },
    });

    if (
      this.state.class_filter.conventional ||
      this.state.class_filter.executive ||
      this.state.class_filter.bed ||
      this.state.class_filter.semi_bed
    ) {
      console.log("foi");
    }
  };

  getTickets = () => {
    const { dispatch } = this.props;
    dispatch(ticketAction.getTicket());
  };

  render() {
    const { ticket } = this.props;

    const {
      sidebarIsOpen,
      favorites,
      class_filter,
      selected,
      exit_filter,
    } = this.state;
    return (
      <div>
        {sidebarIsOpen && (
          <Sidebar
            handleOpenSidebar={this.handleOpenSidebar}
            sidebarIsOpen={sidebarIsOpen}
            favorites={favorites}
            onRemoved={this.onRemoved}
            ticketsCount={favorites.length}
          />
        )}

        <Header
          handleOpenSidebar={this.handleOpenSidebar}
          sidebarIsOpen={sidebarIsOpen}
          ticketsCount={favorites.length}
        />

        <TitleFilter />
        <div className="allpag">
          <div className="content">
            <Table
              ticket={ticket}
              onSelected={this.onSelected}
              favorites={favorites}
              selected={selected}
            />
          </div>
          <div className="filtrospag">
            <FilterBox
              onFiltered={this.onFiltered}
              class_filter={class_filter}
              exit_filter={exit_filter}
            />

            <SelectedPass selected={selected} onConfirmed={this.onConfirmed} />
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { ticket } = state;
  let data = {
    ticket,
  };
  return data;
}

// function mapDispatchToProps(dispatch) {
// console.log("dispatch :>> ", dispatch);
// const { dispatch } = state;
// let data = {
// getAll: () => dispatch(ticketAction.ticket())
// getAll: () => dispatch(ticketAction.getTicket())
// }
// console.log('dispatch(ticketAction.getTicket() :>> ', dispatch(ticketAction.getTicket()))
// console.log('dataiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii :>> ', data)
// return data
// }

export default connect(mapStateToProps)(HomePageComponent);
