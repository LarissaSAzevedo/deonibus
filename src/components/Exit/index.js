import React from "react";

const Saida = (props) => {
  const { dawn, morning, evening, night, onFiltered } = props;
  return (
    <div className="saida">
      <strong>Saída</strong>
      <form className="checks">
        <label>
          <input
            type="checkbox"
            id="madrug"
            name="madrug"
            value={dawn}
            onChange={onFiltered}
          />
          Madrugada - (00h00 - 05h59)
        </label>
        <label>
          <input
            type="checkbox"
            id="manha"
            name="manha"
            value={morning}
            onChange={onFiltered}
          />
          Manhã - (06h00 - 11h59)
        </label>
        <label>
          <input
            type="checkbox"
            id="tarde"
            name="tarde"
            value={evening}
            onChange={onFiltered}
          />
          Tarde - (12h00 - 17h59)
        </label>
        <label>
          <input
            type="checkbox"
            id="noite"
            name="noite"
            value={night}
            onChange={onFiltered}
          />
          Noite - (18h00 - 23h59)
        </label>
      </form>
    </div>
  );
};
export default Saida;
