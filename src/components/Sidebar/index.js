import React from "react";
import CardFavorites from "../CardFavorites";

const Sidebar = ({ favorites, onRemoved, sidebarIsOpen, handleOpenSidebar, event, ticketsCount }) => {

  return (
    <div className="sidebar">
      <a href="#" onClick={() => handleOpenSidebar(event)}>
        Passagens favoritas
      </a>

      {ticketsCount !== 0 ? (
        favorites.map((eachFavorite) => (
          <CardFavorites
            fave={eachFavorite}
            favorites={favorites}
            key={eachFavorite.objectId}
            onRemoved={onRemoved}
            sidebarIsOpen={sidebarIsOpen}
          />
        ))
      ) : (
        <p>Confirme suas passagens para prosseguir</p>
      )}
    </div>
  );
};

export default Sidebar;
