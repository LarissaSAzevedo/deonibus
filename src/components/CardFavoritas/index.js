import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";
import IconButton from "@material-ui/core/IconButton";
import Grid from "@material-ui/core/Grid";
import DeleteIcon from "@material-ui/icons/Delete";
import { parseISO, format } from "date-fns";

const useStyles = makeStyles({
  root: {
    flexGrow: 1,
    maxWidth: 752,
    padding: "1vw",
    paddingTop: "1.5vw",
  },
  infos: {
    fontSize: "12px !important",
  },
  price: {
    fontSize: "16px !important",
  },
  icon: {
    color: "white",
  },
});

const CardFavoritas = (props) => {

  const {} = props

  const classes = useStyles();

  const departureDate = parseISO(props.fave.DepartureDate.iso);
  const dateFormatedDeparture = format(departureDate, "dd/MM/yyyy");

  const departureHour = parseISO(props.fave.DepartureDate.iso);
  const hourFormatedDeparture = format(departureHour, "HH:mm'h'");

  return (
    <div className={classes.root}>
      <Grid item xs={12}>
        <List>
          <ListItem>
            <ListItemText>
              <p className={classes.infos}>Origem: {props.fave.Origin}</p>
              <p className={classes.infos}>Destino: {props.fave.Destination}</p>
              <p className={classes.infos}>
                Embarque: {dateFormatedDeparture} - {hourFormatedDeparture}
              </p>
            </ListItemText>
            <ListItemText>
              <h2 className={classes.price}>
                R$ {props.fave.Price.toLocaleString("pt-BR")}
              </h2>
              <p className={classes.infos}>{props.fave.BusClass}</p>
            </ListItemText>
            <ListItemSecondaryAction
              onClick={() => {
                props.isRemovedFavorites(props.fave.objectId);
              }}
            >
              <IconButton edge="end" aria-label="Remover item da lista">
                <DeleteIcon className={classes.icon} />
              </IconButton>
            </ListItemSecondaryAction>
          </ListItem>
        </List>
      </Grid>
    </div>
  );
};

export default CardFavoritas;
