import React from "react";

import BusClass from "../BusClass";
import Exit from "../Exit";
import FilterPrice from "../FilterPrice";

const FilterBox = (props) => {
  const { class_filter, exit_filter, onFiltered } = props;
  return (
    <div className="filterbox top-box">
      <BusClass onFiltered={onFiltered} class_filter={class_filter} />
      <Exit onFiltered={onFiltered} exit_filter={exit_filter} />
      <FilterPrice />
    </div>
  );
};
export default FilterBox;
