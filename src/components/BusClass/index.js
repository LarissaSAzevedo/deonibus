import React from "react";

const BusClass = (props) => {
  const { conventional, executive, bed, semi_bed, onFiltered } = props;
  return (
    <div className="classe">
      <strong>Classe</strong>
      <form className="checks">
        <label>
          <input
            type="checkbox"
            id="convenional"
            name="convencional"
            value={conventional}
            onChange={onFiltered}
          />
          Convencional
        </label>
        <label>
          <input
            type="checkbox"
            id="executivo"
            name="executivo"
            value={executive}
            onChange={onFiltered}
          />
          Executivo
        </label>

        <label>
          <input
            type="checkbox"
            id="semileito"
            name="semileito"
            value={bed}
            onChange={onFiltered}
          />
          Semi-leito
        </label>

        <label>
          <input
            type="checkbox"
            id="leito"
            name="leito"
            value={semi_bed}
            onChange={onFiltered}
          />
          Leito
        </label>
      </form>
    </div>
  );
};
export default BusClass;
