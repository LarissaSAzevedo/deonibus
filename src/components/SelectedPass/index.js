import React from "react";

const SelectedPass = ({ onConfirmed, selected }) => {
  return (
    <div className="pass-selecionadas top-box">
      <strong>{selected.length}</strong>
      <p>passagens selecionadas</p>
      <button onClick={() => onConfirmed(selected)}>Confirmar favoritas</button>
    </div>
  );
};

export default SelectedPass;
