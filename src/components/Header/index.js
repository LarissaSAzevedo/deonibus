import React from "react";
import MenuImg from "../../assets/img/iconsmenu.svg";
import Heart from "../../assets/img/heart.svg";

const Header = ({ ticketsCount, handleOpenSidebar, event }) => {
  return (
    <div className="header">
      <div className="container">
        <img src={MenuImg} alt="botão de menu" />
        <div className="passagensfav">
          <img src={Heart} alt="Passagens favoritas" />
          <a href="#" onClick={() => handleOpenSidebar(event)}>
            Passagens favoritas ({ticketsCount === 0 ? "0" : ticketsCount})
          </a>
        </div>
      </div>
    </div>
  );
};
export default Header;
