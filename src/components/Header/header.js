import React from 'react';
import MenuImg from '../../assets/img/iconsmenu.svg';
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Icon from "@material-ui/core/Icon";
import Favorite from '@material-ui/icons/Favorite';
import Typography from "@material-ui/core/Typography";
import Link from '@material-ui/core/Link';
import Box from '@material-ui/core/Box';
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";

const useStyles = makeStyles({
  iconsList: {
    color: 'white',
    margin: -30
  },
  bgcolor: {
    backgroundColor: "#E61D41"
  },
  menuButton: {
    marginRight: 2,
    flexGrow: 1,
    justifyContent: "flex-start"
  },
  btn: {
    marginRight: 2,
    flexGrow: 1,
    justifyContent: "flex-end"
  },
  img: {
    width: '30px',
    color: "#000000",
    fill: "#000000",
    padding: '1vw'
  },
  container: {
    display: "flex",
    justifyContent: "space-between",
    width: '80vw',
    margin: 'auto !important',
  },
  colorLink: {
    color: '#ffffff !important'
  }
});

const Header = (props) => {
  const classes = useStyles()

  return (
    <div className="header">
      <AppBar position="static" className={classes.bgcolor}>
        <Toolbar >
          <Box maxWidth="sm" className={classes.container}>
            <img src={MenuImg} alt="botão de menu" className={classes.img} />
            <div>
              <List>
                <ListItem onClick={() => props.isSidebarOpen(props.event)} >
                  <ListItemIcon className={classes.iconsList}>
                    <Icon>
                      <Favorite />
                    </Icon>
                  </ListItemIcon>
                  <ListItemText />
                  <Typography variant="h6">
                    <Link href="#" className={classes.colorLink}>Passagens favoritas {props.favorites.length}</Link>
                  </Typography>
                </ListItem>
              </List>
            </div>
          </Box>
        </Toolbar>
      </AppBar>
    </div>
  );
}
export default Header;