import { TICKETS } from "../constants";
import { mainApis } from "../../services/services/mainApis";

const failure = (actionType, error) => {
  return { type: actionType, error };
};

const getTicket = () => {
  const success = (data) => {
    return { type: TICKETS.TICKETS_GET_SUCCESS, data };
  };

  return (dispatch) => {
    mainApis.getAll().then(
      (res) => {
        dispatch(success(res.results));
      },
      (error) => {
        dispatch(failure(TICKETS.TICKETS_GET_FAILURE, error));
      }
    );
  };
};

export const ticketAction = { getTicket };
