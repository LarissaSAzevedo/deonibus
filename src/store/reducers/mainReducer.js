// REDUCERS
import { ACTIONS } from "../constants/actions";

import initialState from "./initialState";

const mainReducer = (state = initialState, action) => {
  const { type, payload } = action;
  const { passList } = state;

  switch (type) {
    // case ACTIONS.OPEN_SIDEBAR:
    //   return {
    //     ...state,
    //     click: !state.click,
    //   };

    // case ACTIONS.CHOOSE_PASS:
    //   return [...state, state.objectId];

    // case ACTIONS.CONFIRM_PASS:
    //   return {
    //     confirmedPass: !confirmedPass,
    //   };

    case ACTIONS.GET_PASS_LIST:
      return {
        ...state,
        passList: [...passList, payload],
      };

    default:
      return state;
  }
};

export { mainReducer };
