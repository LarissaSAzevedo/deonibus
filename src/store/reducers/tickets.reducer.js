import { TICKETS } from "../constants";

const initialState = { error: null, data: [] };

const ticket = (state = initialState, action) => {
  switch (action.type) {
    case TICKETS.TICKETS_GET_SUCCESS:
      return { ...state, data: action.data };

    case TICKETS.TICKETS_GET_FAILURE:
      return { ...state, error: action.error };

    default:
      return state;
  }
};

export default ticket;
