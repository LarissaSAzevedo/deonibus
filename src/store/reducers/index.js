import { combineReducers } from "redux";
import ticket from "./tickets.reducer";

const appReducer = combineReducers({ ticket });

const rootReducer = (state, action) => {
  return appReducer(state, action);
};

export default rootReducer;
