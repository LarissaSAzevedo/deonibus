import Axios from "axios";

const api = Axios.create({
  baseURL:
    "https://4jehkg0izj.execute-api.us-east-1.amazonaws.com/stage-v0/route",
});

const getRoute = () => {
  return api.get("/");
};

export default {
  getRoute,
};
