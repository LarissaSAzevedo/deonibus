export function abrirSidebar(event) {
    return {
        type: 'OPEN-SIDEBAR',
        event
    }
}

export function carregaLista(listaAt) {
    return {
        type: 'ATUALIZA-LISTA',
        listaAt
    }
}

export function listaSelecionadas(objectId) {
    return{
        type: 'LISTA-SELECIONADAS',
        objectId
    }
}

export function passIsConfirmed(passIsConfirmed){
    return {
        type: 'PASS-IS-CONFIRMED',
        passIsConfirmed
    }
}
