# DeÔnibus

## Teste Front-End 😃

Mobelo interativo (*navegue pelos frames e interaja com o modelo*): [https://xd.adobe.com/spec/5ee482a4-5462-4542-7165-91b83c3b2362-28d5/](https://xd.adobe.com/spec/5ee482a4-5462-4542-7165-91b83c3b2362-28d5/)

### Tasks a serem desenvolvidas

- Fazer o fork deste repositório.
- Listar viagens
    - Destacar viagem com melhor preço
    - Exibir lista ordenada por horário de partida
    - Formatação de valor e data
- Montar filtros levando em conta:
    - Classes disponíveis
    - Saída (Madrugada (00h:00 - 5h:59), Manhã (06h:00 - 11:59), Tarde (12h:00 - 17:59) e Noite (18h:00 - 23:59))
    - Preço máximo até R$ X
- Permitir escolher múltiplas viagens para depois adicionar todas à lista
- O menu lateral deve listar o resumo das viagens favoritas selecionadas
- Ao confirmar, **simular** envio da requisição ao backend e tratamento de sucesso ou erro
- Ao finalizar, subir apenas no seu próprio repositório e nos passar o link. **Não fazer pull request ou commit diretamente neste repositório**. [Como fazer o fork.](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html)

### Informações importantes

- Alguns elementos na tela possuem ação de clique, mostrando como seria o fluxo de ação do usuário 🧐

- A adição das passagens a uma lista de favoritos deve ser feita somente ao clicar o botão *confirmar favoritos*, beleza?

- É necessário utilizar o State Global com Redux para manter a lista de favoritos

- Tente não utilizar packages de terceiros, exceto quando achar muito necessário 😬

- Escrever seu próprio CSS ao invés de frameworks é um diferencial hein... Desfrute de SASS e LESS à vontade

- É preciso usar o React/Redux e ES6, fechado? 😎

- Cá entre nós, se conseguir fazer uma versão responsiva vai ser massa!

- GL HF, ENJOY!  🤟


### Request 

Exemplo funcional no Postman: [DeOnibus-Public-Boilerplate.postman_collection.json](DeOnibus-Public-Boilerplate.postman_collection.json)

Dados da requisição: GET ROUTES

URL: https://4jehkg0izj.execute-api.us-east-1.amazonaws.com/stage-v0/route
Method: GET

Headers

- X-Parse-Application-Id: LtD1wBDjTJB7CcuF3hNRNmvRI9CQpozYzN7jIxfA

- X-Parse-REST-API-Key: 1TMvuvt9n2qHCqdQ8qpLw7DX6wUQpq2zhq0OGTvp

- content-type: application/json;

Response

- Success Status: 200 OK

- Body: JSON com uma lista de objetos.


---

## Boilerplate

Baseado no [React Slingshot](https://github.com/coryhouse/react-slingshot) este boilerplate foi criado para diversas utilizações na DeÔnibus.


## Instalação

Após clonar, navegue até a pasta principal:

```bash
npm install
```

## Iniciar

```bash
npm start
```


# deonibus1
# deonibus1
